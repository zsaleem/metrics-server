## Getting started
In order to run this project in the browser successfully, please follow the steps below!

    1. Clone this repository.
    2. cd into `metrics-server` directory.
    3. Install `nodemon` globally e.g. `npm install nodemon -g`.
    4. Run `yarn/npm install` command to download and install all dependencies.
    5. To run this project use `nodemon index.js` command in command line.

## Table of contents from [wiki page](https://gitlab.com/zsaleem/metrics-server/-/wikis/Home)

1. [Home](https://gitlab.com/zsaleem/metrics-server/-/wikis/Home)
2. [Getting Started](https://gitlab.com/zsaleem/metrics-server/-/wikis/Getting-Started)
3. [Git Work Flow](https://gitlab.com/zsaleem/metrics-server/-/wikis/Git-Work-Flow)
4. [Development Environment](https://gitlab.com/zsaleem/metrics-server/-/wikis/Development-Environment)
5. [Architecture](https://gitlab.com/zsaleem/metrics-server/-/wikis/Architecture)
6. [DevOps](https://gitlab.com/zsaleem/metrics-server/-/wikis/DevOps)

## Live Demo
To view live demo please [click here](https://metric-client.herokuapp.com/).

To view list of CI/CD pipeline [click here](https://gitlab.com/zsaleem/metrics-server/-/pipelines).

To view list of all commits [click here](https://gitlab.com/zsaleem/metrics-server/-/commits/master).

To view list of all branches [click here](https://gitlab.com/zsaleem/metrics-server/-/branches/all).

To view list of all merge request [click here](https://gitlab.com/zsaleem/metrics-server/-/merge_requests?scope=all&state=all).

To view git branch strategy [click here](https://swimlanes.io/u/efcx3LmCi).

To view architecture diagram [click here](https://app.terrastruct.com/diagrams/1166687665#layer=1333222501).

#### Note 1: When first time the UI is loaded in browser, it takes quite a bit of time to load all the data from the AtlasDB database which the backend cache all the data and on the rest of the UI loads the performance will be good as the data will be loaded from cache in backend.

#### Note 2: Run the `metrics-client` project before running this `metrics-server` project for them work successfully.

#### Note 3: I added mock data between `28 Nov - 6 Dec`. Therefore, when testing please choose date between these dates.
