const cache = require('memory-cache');
const Metric = require('../models/Metric');
const User = require('../models/User');

const DAY_IN_MILLI_SECONDS = 1000 * 60 * 60 * 24;

async function Cache() {
	let metrics, users;

	// clear cache once a day to get up to date
	// data from database.
	interval = setInterval(() => {
		console.log('Clearing Cache...');
		cache.clear();
	}, DAY_IN_MILLI_SECONDS);

	if (!cache.get('metrics') && !cache.get('users')) {
		try {
			console.log('Retreving data from database.');
			metrics = await Metric.find();
			users = await User.find();

			console.log('Caching data.');
			cache.put('metrics', metrics);
			cache.put('users', users);

			return cache;
		} catch(error) {
			console.log('error: ', error);
			return error;
		}
	} else {
		console.log('Returning cached data.');
		return cache;
	}
};

module.exports = {
	Cache,
};
