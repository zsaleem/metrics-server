'use strict';

const express = require('express');
const metricsService = require('../../services/metrics/metrics');

let router = express.Router();

router.get('/', metricsService.getMetrics);

module.exports = router;
