'use strict';

const moment = require('moment');
const Cache = require('../../cache/cache');

const filterMetricsUponDivisor = (metric, divisor, date, hour, minute) => {
	if (divisor === 'day' && moment(metric.timestamp).date() === moment(date).date()) {
		return metric;
	}
	if (divisor === 'hour' && moment(metric.timestamp).hours() == hour) {
		return metric;
	}
	if (divisor === 'minute' && moment(metric.timestamp).minutes() == minute) {
		return metric;
	}
};

const sumMetricsValues = (acc, metric) => {
	if (acc.hasOwnProperty('value')) {
		return acc.value + metric.value;
	} else {
		return acc + metric.value;
	}
};

const filterUserValues = (metrics, users) => {
	let usersWithMetrics = [];

	users.map((user, index) => {
		metrics.filter(metric => {
			if (metric.name === user.name.toLowerCase()) {
				const isUserInMetrics = usersWithMetrics.some(d => {
					return d.name === metric.name;
				});

				if (usersWithMetrics.length === 0 || !isUserInMetrics) {
					usersWithMetrics.push({
						name: metric.name,
						metrics: [
							{
								value: metric.value,
								timestamp: metric.timestamp,
							}
						]
					})
				} else {
					usersWithMetrics[index]
						.metrics
						.push({
							value: metric.value,
							timestamp: metric.timestamp,
						});
				}
			}
		})
	})

	return usersWithMetrics;
};

const calculateMetricsAverage = (metrics) => {
	return metrics.sum / metrics.metrics.length;
};

// 28 Nov - 6 Dec Data added
async function getMetrics(request, response) {
	const { date, divisor, hour, minute } = request.query;
	let usersWithAverageValueAndTimestamp, metrics;

	if (moment(date) < moment('11/28/2021') || moment(date) > moment('12/07/2021')) {
		return response.json({
			error: true,
			message: 'Please choose dates between "28 November, 2021" & "06 December, 2021".',
		});
	}

	metrics = await Cache.Cache();

	// filter metrics based on day or hour or minute.
	const filteredMetrics = metrics.get('metrics')
		.filter(metric => filterMetricsUponDivisor(metric, divisor, date, hour, minute));
	const usersMetrics = filterUserValues(filteredMetrics, metrics.get('users'));

	const userWithSummedMetrics = usersMetrics.map(userMetric => {
		const sum = userMetric.metrics.reduce(sumMetricsValues);
		userMetric.sum = sum;

		return userMetric;
	});

	usersWithAverageValueAndTimestamp = userWithSummedMetrics.map(metric => {
		const userAverage = calculateMetricsAverage(metric);
		metric['value'] = userAverage;
		metric['timestamp'] = Date.now();
		delete metric.sum;
		delete metric.metrics;

		return metric;
	});

  response.json(usersWithAverageValueAndTimestamp);
}

module.exports = {
  getMetrics,
};
