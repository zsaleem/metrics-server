const mongoose = require('mongoose');

const MetricSchema = new mongoose.Schema({
  name: {
    type: String,
    lowercase: true,
    required: true
  },
  value: {
    type: Number,
    required: true
  },
  timestamp: {
  	type: Number,
  	required: true,
  }
});

module.exports = mongoose.model('Metric', MetricSchema);
