const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    lowercase: true,
    required: true
  },
});

module.exports = mongoose.model('User', UserSchema);
