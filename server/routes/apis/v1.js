'use strict';

const metricsController = require('../../controllers/apis/metrics');
const express = require('express');

let router = express.Router();

router.use('/metrics', metricsController);

module.exports = router;